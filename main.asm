%include "words.inc"
%include "lib.inc"

extern find_word

section .data

message: db 'Enter a key to search: ', 0
search_word: times 256 db 0 
error: db 'Invalid input', 0

section .text

global _start

_start:
    mov rdi, message
    call print_string

    mov rdi, search_word
    mov rsi, 255
    call read_word

    cmp rax, 0
    jne .search

    .input_failure:
        mov rsi, error
        mov rdx, 13
        mov rax, 1
        mov rdi, 2
        syscall

        jmp .finish

    .search:
        mov rdi, word1
        mov rsi, search_word
        call find_word

    .finish:
        call print_newline
        call exit