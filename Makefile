TARGET	= dictionary
LINKER	= ld

ASM_EXT	= ".asm"
OBJ_EXT	= ".o"

all: main.o
	ld -o ${TARGET} "main.o" "lib.o" "dict.o"

main.o:
	nasm -g "main${ASM_EXT}" -felf64 -o "main${OBJ_EXT}"
	nasm -g "lib${ASM_EXT}" -felf64 -o "lib${OBJ_EXT}"
	nasm -g "dict${ASM_EXT}" -felf64 -o "dict${OBJ_EXT}"
    
clean:
	rm -f ./*.o ./${TARGET}

