%include "words.inc"
%include "lib.inc"

section .data

error_1: db 'Key not found', 0

global find_word

section .text

; rdi - dictionary pointer, rsi - string pointer
find_word:
    mov r8, rdi
    mov r9, rsi

	.search_loop:
		; extract the key and compare it
		lea rax, [r8 + 8]
		mov rdi, rax
		mov rsi, r9
		call string_equals
		cmp rax, 1
		je .print_description
	
		; jump to the next item or break the loop if there aare no more items
		mov qword r8, [r8]
		cmp qword r8, 0
		je .key_not_found
		
		jmp .search_loop
		
	.key_not_found:
		; put the error message into stderr
		mov rsi, error_1
        mov rdx, 13
        mov rax, 1
        mov rdi, 2
        syscall
		jmp .finish
	
	.print_description:
		; get the description and print it into stdout
		call string_length
		add rax, 9				; 8 is the next pointer size and 1 is the key terminator 
		lea rax, [r8 + rax]
		mov rdi, rax
		call print_string

    .finish:
        ret